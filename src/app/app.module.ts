import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { PostPage } from '../pages/post/post';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { PasswordPage } from '../pages/password/password';
import { WelcomePage } from '../pages/welcome/welcome';
import { ProfilePage } from '../pages/profile/profile';
import { QuestionPage } from '../pages/question/question';
import { QuestiondetailsPage } from '../pages/questiondetails/questiondetails';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service';
import { MomentModule } from 'angular2-moment';
import { LinkyModule } from 'angular-linky';
import { CommonProvider } from '../providers/common';
import { MyService } from '../providers/my-service';
import { Keyboard } from '@ionic-native/keyboard';
import { DatasourceProvider } from '../providers/datasource';
import { ServerProvider } from '../providers/server';
import { FaqDatasourceProvider } from '../providers/faq-datasource';
import { PostDatasourceProvider } from '../providers/post-datasource';
import { AnnouncementModalPage } from '../pages/announcement-modal/announcement-modal';
import { FaqModalPage } from '../pages/faq-modal/faq-modal';
import { ProfileInfoPage } from '../pages/profile-info/profile-info';
import { ProfileEditPage } from '../pages/profile-edit/profile-edit';
import { NotificationsPage } from '../pages/notifications/notifications';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    PostPage,
    TabsPage,
    LoginPage,
    PasswordPage,
    WelcomePage,
    ProfilePage,
    QuestionPage,
    QuestiondetailsPage,
    AnnouncementModalPage,
    FaqModalPage,
    ProfileInfoPage,
    ProfileEditPage,
    NotificationsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MomentModule,
    LinkyModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    PostPage,
    TabsPage,
    LoginPage,
    PasswordPage,
    WelcomePage,
    ProfilePage,
    QuestionPage,
    QuestiondetailsPage,
    AnnouncementModalPage,
    FaqModalPage,
    ProfileInfoPage,
    ProfileEditPage,
    NotificationsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    MyService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthServiceProvider,
    CommonProvider,
    DatasourceProvider,
    ServerProvider,
    FaqDatasourceProvider,
    PostDatasourceProvider
  ]
})
export class AppModule { }
