import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the FaqModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-faq-modal',
  templateUrl: 'faq-modal.html'
})
export class FaqModalPage {
  public pastQuestion = '';
  public pastAnswer = '';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    const params = this.navParams.get('item');
    console.log(params);
    if (params) {
      this.pastQuestion = params.question;
      this.pastAnswer = params.answer;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionPage');
  }

  post(question, answer) {
    // Returning data from the modal:
    this.viewCtrl.dismiss({
      question: question.value,
      answer: answer.value
    });
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
}
