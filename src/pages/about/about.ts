import { Component } from '@angular/core';
import {
  NavController,
  App,
  AlertController,
  NavParams,
  MenuController,
  ModalController
} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service';
import { CommonProvider } from '../../providers/common';
import { QuestionPage } from '../question/question';
import { QuestiondetailsPage } from '../questiondetails/questiondetails';
import { ServerProvider } from '../../providers/server';
import { PostDatasourceProvider } from '../../providers/post-datasource';
import { ProfileInfoPage } from '../profile-info/profile-info';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  // timeline:any;
  // scrollLocation: any;
  posts: any;
  userDetails;

  constructor(
    public db: ServerProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public app: App,
    public alertCtrl: AlertController,
    public commonProvider: CommonProvider,
    public authServiceProvider: AuthServiceProvider,
    public datasource: PostDatasourceProvider
  ) {
    // this.timeline = navParams.get('timeline');
    this.getPosts();
  }

  ionViewWillEnter() {
    console.log('Will run every time');

    // this.getPosts();
  }

  getPosts() {
    this.db.getData('http://localhost:8000/api/post/list').subscribe(data => {
      console.log(data);
      this.datasource.source = data.success;
    });
    const data = JSON.parse(localStorage.getItem('token'));
    this.userDetails = data.data.userData;
  }

  // like(timeline) {
  //     if ((timeline.liked == false) || (timeline.liked == undefined)) {
  //         timeline.liked = true;
  //         //code for adding liked post to server
  //     } else {
  //         timeline.liked = false;
  //     }
  // }

  addQuestion() {
    let modal = this.modalCtrl.create(QuestionPage);
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.createItem(data);
      }
    });
    modal.present();
  }

  createItem(data) {
    // console.log(data);

    let i = {
      title: data.title,
      content: data.content,
      role: data.role,
      user_id: this.userDetails.id
    };
    // console.log(i);
    this.db
      .create(i, 'http://localhost:8000/api/post/detail')
      .subscribe(data => {
        console.log(data);
        this.datasource.source.push(data.success);
        // this.getPosts();
      });
  }

  deleteItem(item) {
    this.db
      .delete(item, 'http://localhost:8000/api/post/detail/')
      .subscribe(data => {
        console.log(data);
        let index = this.datasource.source.indexOf(item);
        this.datasource.source.splice(index, 1);
      });
  }

  editItem(item) {
    let modal = this.modalCtrl.create(QuestionPage, { item: item });
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.updateItem(data);
      }
    });
    modal.present();
    this.datasource.tempdata = item;
  }

  updateItem(data) {
    let i = {
      id: this.datasource.tempdata.id,
      title: data.title,
      content: data.content,
      role: data.role,
      user_id: this.userDetails.id
    };
    this.db
      .update(i, 'http://localhost:8000/api/post/detail')
      .subscribe(data => {
        console.log(data);
        let index = this.datasource.source.indexOf(this.datasource.tempdata);
        this.datasource.source[index] = data.success;
      });
  }

  details(item) {
    // console.log(item);
    this.navCtrl.push(QuestiondetailsPage, { postData: item });
  }

  showProfileInfo(item) {
    console.log(item.user);
    this.navCtrl.push(ProfileInfoPage, { item: item.user });
  }
}
