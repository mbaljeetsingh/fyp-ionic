import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, AlertController, ViewController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service';
import { CommonProvider } from '../../providers/common';
import { HomePage } from '../home/home';

/**
 * Generated class for the PostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {

  public userDetails: any;
  responseData: any;
  dataSet: any;
  userPostData = { "user_id": "", "token": "", "title": "", "feed": "", "feed_id": "" };

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public app: App, public alertCtrl: AlertController, public commonProvider: CommonProvider, public authServiceProvider: AuthServiceProvider) {
    const data = JSON.parse(localStorage.getItem('token'));
    this.userDetails = data.data.userData;
    this.userPostData.user_id = this.userDetails.id;
    this.userPostData.token = data.data['user-access-token'];
    this.getFeed();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostPage');
  }

  getFeed() {
    this.commonProvider.presentLoading();
    this.authServiceProvider.postData(this.userPostData, 'feed')
      .then((result) => {
        this.responseData = result;
        if (this.responseData.feedData) {
          this.commonProvider.closeLoading();
          this.dataSet = this.responseData.feedData;
          console.log(this.dataSet);
        } else { }
      }, (err) => {

      });
  }

  feedUpdate() {
    if (this.userPostData.feed) {
      this.commonProvider.presentLoading();
      this.authServiceProvider.postData(this.userPostData, "feedUpdate")
        .then((result) => {
          this.responseData = result;
          if (this.responseData.feedData) {
            this.commonProvider.closeLoading();
            this.dataSet.unshift(this.responseData.feedData);
            this.userPostData.feed = "";
            this.viewCtrl.dismiss();
            console.log(this.dataSet);
          } else {
            console.log("No access");
          }

        }, (err) => {
          //Connection failed message
        });
    }
  }

  convertTime(created) {
    let date = new Date(created * 1000);
    return date;
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
}
