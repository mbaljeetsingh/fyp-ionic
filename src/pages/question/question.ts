import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';

/**
 * Generated class for the QuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question',
  templateUrl: 'question.html'
})
export class QuestionPage {
  public pastTitle = '';
  public pastContent = '';
  public pastRole = 0;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    const params = this.navParams.get('item');
    console.log(params);
    if (params) {
      this.pastTitle = params.title;
      this.pastContent = params.content;
      this.pastRole = params.role;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionPage');
  }

  post(title, content, role) {
    // Returning data from the modal:
    this.viewCtrl.dismiss({
      title: title.value,
      content: content.value,
      role: role.value
    });
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
}
