import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Slides, Content, ToastController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { AuthServiceProvider } from '../../providers/auth-service';

/**
 * Generated class for the PasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-password',
  templateUrl: 'password.html',
})
export class PasswordPage {

  public height;
  responseData: any;
  userData = { "name": "", "student_id": "", "password": "", "confirm_password": "" };

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
     public viewCtrl: ViewController, 
     public keyboard: Keyboard,    
     public authServiceProvider: AuthServiceProvider,
     public toastCtrl: ToastController) {
    this.signupKeyboard();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasswordPage');
  }

  ionViewDidEnter() {
    this.height = ((window.innerHeight * 65) / 100) - 60; //Do not change the value
  }

  signupKeyboard() {
    this.keyboard.onKeyboardShow().subscribe(
      data => {
        if (document.getElementById('signUp')) {
          document.getElementById('signUp').style.height = this.height + 'px';
        }
      }
    );
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  signup() {
    if (this.userData.name && this.userData.student_id && this.userData.password && this.userData.confirm_password) {
      console.log(this.userData);
      if(this.userData.password != this.userData.confirm_password) {
        this.presentToast("Passwords didn\'t match");
        return;
      }
      this.authServiceProvider.postData(this.userData, 'forgot').then((result) => {
        this.responseData = result;
        console.log(this.responseData);
        if (this.responseData.success) { // check status http
          this.presentToast("Password updated");
          this.viewCtrl.dismiss();
        } else {
          this.presentToast("Pls give valid name and student Id");
        }
      }, (err) => {
        // Error log
      });
    } else {
      this.presentToast("Give valid inputs");
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
