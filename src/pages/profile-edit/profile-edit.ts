import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html',
})
export class ProfileEditPage {

  public pastName = '';
  public pastEmail = '';
  public pastRole = 0;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    const params = this.navParams.get('item');
    console.log(params);
    if (params) {
      this.pastName = params.name;
      this.pastEmail = params.email;
      this.pastRole = params.role;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionPage');
  }

  post(name, email, role) {
    // Returning data from the modal:
    this.viewCtrl.dismiss({
      name: name.value,
      email: email.value,
      role: role.value
    });
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }

}
