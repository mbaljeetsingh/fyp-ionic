import { Component } from '@angular/core';
import { ViewController, AlertController, ToastController, IonicPage, NavController, NavParams} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { AuthServiceProvider } from '../../providers/auth-service';
import { Keyboard } from '@ionic-native/keyboard';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public myToast: any;
  public icon: any;
  public height;
  responseData: any;
  userData = { "email": "", "password": "" };

  constructor(public viewCtrl: ViewController,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public keyboard: Keyboard,
    public authServiceProvider: AuthServiceProvider,
    public toastCtrl: ToastController) {
    this.loginkeyboard();
  }

  ionViewDidEnter() {
    this.height = ((window.innerHeight * 65) / 100) - 60; //Do not change the value
  }

  loginkeyboard() {
    this.keyboard.onKeyboardShow().subscribe(
      data => {
        if (document.getElementById('login')) {
          document.getElementById('login').style.height = this.height + 'px';
        }
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  login() {
    if (this.userData.email && this.userData.password) {
      this.authServiceProvider.postData(this.userData, 'login').then((result) => {
        this.responseData = result;
        console.log(this.responseData);
        if (this.responseData.data.code == '200') { // check status http
          localStorage.setItem('token', JSON.stringify(this.responseData));
          this.navCtrl.push(TabsPage);
        } else {
          this.presentToast("Pls give valid username and password");
        }
      }, (err) => {
        // Error log
      });
    } else {
      this.presentToast("Give valid username and password");
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
