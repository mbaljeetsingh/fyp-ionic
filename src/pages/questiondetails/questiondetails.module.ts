import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestiondetailsPage } from './questiondetails';

@NgModule({
  declarations: [
    QuestiondetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestiondetailsPage),
  ],
})
export class QuestiondetailsPageModule {}
