import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServerProvider } from '../../providers/server';
import { PostDatasourceProvider } from '../../providers/post-datasource';
import { ProfileInfoPage } from '../profile-info/profile-info';

/**
 * Generated class for the QuestiondetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questiondetails',
  templateUrl: 'questiondetails.html'
})
export class QuestiondetailsPage {
  item;
  userDetails;
  pastComment = '';

  liked = false;

  constructor(
    public db: ServerProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public datasource: PostDatasourceProvider
  ) {
    this.item = this.navParams.get('postData');
    console.log(this.item);
    const data = JSON.parse(localStorage.getItem('token'));
    this.userDetails = data.data.userData;

    const post_id = this.item.id;
    const user_id = this.userDetails.id;

    const likesFilter = this.item.likes.filter(function(like) {
      return like.post_id === post_id && like.user_id === user_id;
    });
    // console.log(likesFilter);
    this.liked = likesFilter.length > 0 ? true : false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestiondetailsPage');
  }

  addComment(comment) {
    console.log(comment.value);
    let i = {
      post_id: this.item.id,
      user_id: this.userDetails.id,
      content: comment.value
    };
    // console.log(i);
    this.db
      .create(i, 'http://localhost:8000/api/post/comment')
      .subscribe(data => {
        console.log(data);
        let index = this.datasource.source.indexOf(this.item);
        this.datasource.source[index].comments.push(data.success);
        this.pastComment = '';
        // this.getPosts();
      });
  }

  addLike() {
    this.liked = true;
    let i = {
      post_id: this.item.id,
      user_id: this.userDetails.id
    };
    // console.log(i);
    this.db.create(i, 'http://localhost:8000/api/post/like').subscribe(data => {
      console.log(data);
      let index = this.datasource.source.indexOf(this.item);
      this.datasource.source[index].likes.push(data.success);
      // this.liked = true;
      // this.getPosts();
    });
  }

  removeLike() {}

  showProfileInfo(user) {
    this.navCtrl.push(ProfileInfoPage, { item: user });
  }
}
