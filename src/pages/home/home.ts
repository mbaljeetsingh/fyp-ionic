import { forwardRef, Component } from '@angular/core';
import {
  NavController,
  App,
  AlertController,
  MenuController,
  ModalController,
  NavParams
} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service';
// import { CommonProvider } from '../../providers/common';
// import { AboutPage } from '../about/about';
import { PostPage } from '../post/post';
import { ServerProvider } from '../../providers/server'; // service for crud with server side
import { DatasourceProvider } from '../../providers/datasource'; // to store data for view
import { CommonProvider } from '../../providers/common';
import { AnnouncementModalPage } from '../announcement-modal/announcement-modal';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [forwardRef(() => ServerProvider)]
  // providers:[ServerProvider]
})
export class HomePage {
  public items: any;
  public toggle: boolean = false;
  public pastname: string;
  public pastrole = 0;
  public userDetails: any;
  // responseData: any;
  // dataSet: any;
  // public noRecord: boolean;
  // userPostData = { "user_id": "", "token": "", "feed": "", "feed_id": "", "lastCreated": "" };

  constructor(
    public navCtrl: NavController,
    public app: App,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public commonProvider: CommonProvider,
    public authServiceProvider: AuthServiceProvider,
    public db: ServerProvider,
    public navparam: NavParams,
    public datasource: DatasourceProvider
  ) {
    this.db.getData('http://localhost:8000/api/task/list').subscribe(data => {
      console.log(data);
      this.datasource.source = data.success;
    });
    const data = JSON.parse(localStorage.getItem('token'));
    this.userDetails = data.data.userData;
    //  this.userPostData.user_id = this.userDetails.id;
    //  this.userPostData.token = data.data['user-access-token'];
    // this.userPostData.lastCreated = '';
    // this.noRecord = false;
    // this.getFeed();
  }

  addAnnouncement() {
    let modal = this.modalCtrl.create(AnnouncementModalPage);
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.createItem(data);
      }
    });
    modal.present();
  }

  createItem(data) {
    // console.log(name.value, role.value);

    let i = { title: data.name, role: data.role };
    this.db
      .create(i, 'http://localhost:8000/api/task/detail')
      .subscribe(data => {
        console.log(data);
        this.datasource.source.push(data.success);
      });
  }

  deleteItem(item) {
    this.db
      .delete(item, 'http://localhost:8000/api/task/detail/')
      .subscribe(data => {
        console.log(data);
        let index = this.datasource.source.indexOf(item);
        this.datasource.source.splice(index, 1);
      });
  }

  // editItem(item) {
  //   this.toggle = true;
  //   this.pastname = item.title;
  //   this.pastrole = item.role;
  //   this.datasource.tempdata = item;
  // }

  editItem(item) {
    console.log(item);
    let modal = this.modalCtrl.create(AnnouncementModalPage, { item: item });
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.updateItem(data);
      }
    });
    modal.present();
    this.datasource.tempdata = item;
  }

  updateItem(data) {
    console.log(data.name, data.role);
    let i = {
      id: this.datasource.tempdata.id,
      title: data.name,
      role: data.role
    };
    this.db
      .update(i, 'http://localhost:8000/api/task/detail')
      .subscribe(data => {
        console.log(data);
        let index = this.datasource.source.indexOf(this.datasource.tempdata);
        this.datasource.source[index] = i;
      });
  }

  // getFeed() {
  //   this.commonProvider.presentLoading();
  //   this.authServiceProvider.postData(this.userPostData, 'feed')
  //     .then((result) => {
  //       this.responseData = result;
  //       if (this.responseData.feedData) {
  //         this.commonProvider.closeLoading();
  //         this.dataSet = this.responseData.feedData;
  //         console.log(this.dataSet);
  //         let dataLength = this.dataSet.length;
  //         this.userPostData.lastCreated = this.dataSet[dataLength - 1].created;
  //         console.log("last created" + this.userPostData.lastCreated);
  //       } else { }
  //     }, (err) => {
  //
  //     });
  // }

  // doInfinite(): Promise<any> {
  //   console.log('Begin async operation');
  //
  //   return new Promise((resolve) => {
  //     setTimeout(() => {
  //       this.authServiceProvider.postData(this.userPostData, 'feed')
  //         .then((result) => {
  //           this.responseData = result;
  //           if (this.responseData.feedData.length) {
  //             const newData = this.responseData.feedData;
  //             console.log("Infinite scroll data");
  //             this.userPostData.lastCreated = newData[newData.length - 1].created;
  //
  //             for (let i = 0; i < newData.length; i++) {
  //               this.dataSet.push(newData[i]);
  //             }
  //
  //           } else {
  //             this.noRecord = true;
  //           }
  //         }, (err) => {
  //
  //         });
  //
  //       console.log('Async operation has ended');
  //       resolve();
  //     }, 500);
  //   })
  // }

  // feedUpdate() {
  //   if (this.userPostData.feed) {
  //     this.commonProvider.presentLoading();
  //     this.authServiceProvider.postData(this.userPostData, "feedUpdate")
  //       .then((result) => {
  //         this.responseData = result;
  //         if (this.responseData.feedData) {
  //           this.commonProvider.closeLoading();
  //           this.dataSet.unshift(this.responseData.feedData);
  //           this.userPostData.feed = "";
  //         } else {
  //           console.log("No access");
  //         }
  //
  //       }, (err) => {
  //         //Connection failed message
  //       });
  //   }
  //
  // }
  //
  // feedDelete(feed_id, msgIndex) {
  //   if (feed_id > 0) {
  //     let alert = this.alertCtrl.create({
  //       title: 'Delete Feed',
  //       message: 'Do you want to buy this feed?',
  //       buttons: [
  //         {
  //           text: 'Cancel',
  //           role: 'cancel',
  //           handler: () => {
  //             console.log('Cancel clicked');
  //           }
  //         }, {
  //           text: 'Delete',
  //           handler: () => {
  //             this.userPostData.feed_id = feed_id;
  //             this.authServiceProvider.postData(this.userPostData, "feedDelete")
  //               .then((result) => {
  //                 this.responseData = result;
  //                 if (this.responseData.success) {
  //                   this.dataSet.splice(msgIndex, 1);
  //                 } else {
  //                   console.log("No access");
  //                 }
  //               }, (err) => {
  //                 //Connection failed message
  //               });
  //           }
  //         }
  //       ]
  //     });
  //     alert.present();
  //   }
  // }
  //
  // convertTime(created) {
  //   let date = new Date(created * 1000);
  //   return date;
  // }

  addPost() {
    let modal = this.modalCtrl.create(PostPage);
    modal.present();
  }
}
