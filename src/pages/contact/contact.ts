import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ServerProvider } from '../../providers/server';
import { FaqDatasourceProvider } from '../../providers/faq-datasource';
import { FaqModalPage } from '../faq-modal/faq-modal';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  shownGroup = null;
  public userDetails: any;
  public toggle: boolean = false;
  public pastquestion: string;
  public pastanswer: string;

  constructor(
    public navCtrl: NavController,
    public db: ServerProvider,
    public modalCtrl: ModalController,
    public datasource: FaqDatasourceProvider
  ) {
    this.db.getData('http://localhost:8000/api/faq/list').subscribe(data => {
      console.log(data);
      this.datasource.source = data.success;
    });
    const data = JSON.parse(localStorage.getItem('token'));
    this.userDetails = data.data.userData;
  }
  ionViewDidLoad() {
    console.log('Hello SettingsPage Page');
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  addFaq() {
    let modal = this.modalCtrl.create(FaqModalPage);
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.createItem(data);
      }
    });
    modal.present();
  }

  createItem(data) {
    // console.log(name.value, role.value);

    let i = { question: data.question, answer: data.answer };
    this.db
      .create(i, 'http://localhost:8000/api/faq/detail')
      .subscribe(data => {
        console.log(data);
        this.datasource.source.push(data.success);
      });
  }

  deleteItem(item) {
    this.db
      .delete(item, 'http://localhost:8000/api/faq/detail/')
      .subscribe(data => {
        console.log(data);
        let index = this.datasource.source.indexOf(item);
        this.datasource.source.splice(index, 1);
      });
  }

  editItem(item) {
    console.log(item);
    let modal = this.modalCtrl.create(FaqModalPage, { item: item });
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.updateItem(data);
      }
    });
    modal.present();
    this.datasource.tempdata = item;
  }

  updateItem(data) {
    // console.log(name.value, role.value);
    let i = {
      id: this.datasource.tempdata.id,
      question: data.question,
      answer: data.answer
    };
    this.db
      .update(i, 'http://localhost:8000/api/faq/detail')
      .subscribe(data => {
        console.log(data);
        let index = this.datasource.source.indexOf(this.datasource.tempdata);
        this.datasource.source[index] = i;
      });
  }
}
