import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServerProvider } from '../../providers/server';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  public items: any;
  public userDetails: any;

  constructor(
    public navCtrl: NavController,
    public db: ServerProvider,
    public navparam: NavParams
  ) {
    this.getData();
    const data = JSON.parse(localStorage.getItem('token'));
    this.userDetails = data.data.userData;
  }

  getData() {
    this.db.getData('http://localhost:8000/api/user/notifications').subscribe(data => {
      console.log(data);
      this.items = data.success;
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

  ionViewWillEnter() {
    this.getData();
  }

}
