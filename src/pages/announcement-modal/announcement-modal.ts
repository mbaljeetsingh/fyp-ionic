import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';

/**
 * Generated class for the AnnouncementModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-announcement-modal',
  templateUrl: 'announcement-modal.html'
})
export class AnnouncementModalPage {
  public pastName = '';
  public pastRole = 0;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    const params = this.navParams.get('item');
    console.log(params);
    if (params) {
      this.pastName = params.title;
      this.pastRole = params.role;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionPage');
  }

  post(name, role) {
    // Returning data from the modal:
    this.viewCtrl.dismiss({
      name: name.value,
      role: role.value
    });
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
}
