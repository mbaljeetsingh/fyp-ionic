import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController } from 'ionic-angular';
import { ICON } from '../../providers/constant';
import { PasswordPage } from '../password/password';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  icon: any;
  myIcon: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public modalCtrl: ModalController) {
    if (localStorage.getItem('token')) {
      this.navCtrl.setRoot(TabsPage);
    }
    this.icon = ICON;
    menuCtrl.enable(false);

  }

  ionViewDidEnter() {
    this.myIcon = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

  signIn() {
    this.myIcon = false;
    let loginModal = this.modalCtrl.create(LoginPage);
    loginModal.present();
    loginModal.onDidDismiss((data) => {
      this.myIcon = true;
      console.log(data);
      if (data) {
        this.navCtrl.setRoot(HomePage);
      }
    })
  }

  password() {
    this.myIcon = false;
    let passwordModal = this.modalCtrl.create(PasswordPage);
    passwordModal.present();
    passwordModal.onDidDismiss((data) => {
      this.myIcon = true;
      if (data) {
        this.navCtrl.setRoot(HomePage);
      }
    })
  }
}
