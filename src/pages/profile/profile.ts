import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Content, App } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service';
import { MyService } from '../../providers/my-service';
import { WelcomePage } from '../welcome/welcome';
import { ProfileEditPage } from '../profile-edit/profile-edit';
import { ServerProvider } from '../../providers/server';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  @ViewChild(Content) content: Content;
  // public profile;
  // public title;
  // public posts;
  // public photos;
  // public show:any;
  // public allowPost: boolean;
  public userDetails: any;

  constructor(public db: ServerProvider, public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public myService: MyService, public app: App, public authServiceProvider: AuthServiceProvider) {
    const data = JSON.parse(localStorage.getItem('token'));
    console.log(data);
    this.userDetails = data.data.userData;
    // console.log(this.userDetails);
    // console.log(userDetails.name);
    // this.show = "posts";
    // if (navParams.get('myProfile') != undefined) {
    //     this.profile = navParams.get('myProfile')[0];
    //     this.title = "My Profile";
    //     this.allowPost = true;
    // } else {
    //     this.profile = navParams.get('other');
    //     console.log("profile: ", this.profile);
    //     this.title = "View Profile";
    //     this.allowPost = false;
    // }
    // myService.getTimelines().subscribe((data) => {
    //     this.posts = data.timelines;
    //     this.photos = data.photos;
    // });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  backToWelcome() {
    const root = this.app.getRootNav();
    root.setRoot(WelcomePage);
    // root.popToRoot();
  }

  logout() {
    // localStorage.clear();
    localStorage.removeItem('token');
    setTimeout(() => this.backToWelcome(), 1000);
  }

  editItem(item) {
    let modal = this.modalCtrl.create(ProfileEditPage, { item: item });
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.updateItem(data);
      }
    });
    modal.present();
    // this.datasource.tempdata = item;
  }

  updateItem(data) {
    let profileData = JSON.parse(localStorage.getItem('token'));

    profileData.data.userData.name = data.name;
    profileData.data.userData.email = data.email;
    profileData.data.userData.role = data.role;
    // console.log(profileData);

    localStorage.setItem('token', JSON.stringify(profileData));


    let i = {
      id: this.userDetails.id,
      name: data.name,
      email: data.email,
      role: data.role
    };
    this.db
      .update(i, 'http://localhost:8000/api/user/profile')
      .subscribe(result => {
        console.log(result);
        const token = JSON.parse(localStorage.getItem('token'));
        this.userDetails = token.data.userData;
      });
  }

}
