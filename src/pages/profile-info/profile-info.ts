import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-profile-info',
  templateUrl: 'profile-info.html'
})
export class ProfileInfoPage {
  public userDetails: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.userDetails = this.navParams.get('item');
    console.log(this.userDetails);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileInfoPage');
  }
}
