import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import { DatasourceProvider } from './datasource';
import 'rxjs/add/operator/map';

/*
  Generated class for the ServerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServerProvider {
  token: any;

  constructor(public http: Http,public datasource: DatasourceProvider) {
    console.log('Hello ServerProvider Provider');
    this.token = JSON.parse(localStorage.getItem('token'));
    // console.log(this.token.data['user-access-token']);
  }

  getData(url){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Authorization', `Bearer ${this.token.data['user-access-token']}`)

    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option)
    .map(res => res.json());

  }

  create(item, url):any {
    let result:any;
    let headers = new Headers();

    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', `Bearer ${this.token.data['user-access-token']}`)

    let option = new RequestOptions({headers: headers});

    return this.http.post(url, JSON.stringify(item),option)
    .map(res => res.json() );

    //return result;
  }

  update(item, url):any {
    let result:any;
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', `Bearer ${this.token.data['user-access-token']}`)

    return this.http.put(url, JSON.stringify(item), { headers: headers })
    .map(res => res.json());
  }

  delete(item, url):any {
    let result:any;
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', `Bearer ${this.token.data['user-access-token']}`)

    return this.http.delete(url+item,{headers: headers})
    .map(res => res.json());
  }

}
