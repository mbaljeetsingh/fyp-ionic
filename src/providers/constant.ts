export let IMAGE_BASEURL = "assets/img";
export let DATA_BASEURL = "assets/data";
//image
export let ICON = IMAGE_BASEURL + "/icon.svg";

//data
export let TIMELINES = DATA_BASEURL + "/timeline.json";
export let PROFILES = DATA_BASEURL + "/profile.json";
export let USERS = DATA_BASEURL + "/users.json";
export let NOTIFICATIONS = DATA_BASEURL + "/notifications.json";

